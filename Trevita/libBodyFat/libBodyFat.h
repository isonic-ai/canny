//
//  libBodyFat.h
//  libBodyFat
//
//  Created by xue lin@cannyscale.com on 2019/2/14.
//  Copyright © 2019年 xue. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface libBodyFat : NSObject <RCTBridgeModule>
/*
 1.sex:  if female,sex=ture else sex=flase
 2.imp:  the impedance of body, get the impedance from the device
 3. althlevel:  let althlevel=0, keep default value!
 4,age: the user age
 5,cm: the user height in cm
 6,wtkg: the user weight in kg
 */

+(NSString *)getVersion;
+(NSString *)getAuthToken;
+(float)getFat:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getTbw:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getMus:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getBone:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getKcal:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getVfat:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getBage:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getBestWeight:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getBMI:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getProtein:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
+(float)getWeightWithoutFat:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
//evaluate the obese rate
// obeserate=(actual weight - best weight)/best weight
/*
 obeserate <10.0 healthy
 obeserate<20.0 over weight
 obeserate<30.0 mildly
 obeserate< 50.0 moderate
 obeserate>50 severe
 */
+(float)getObeseRate:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
//evaluate body score, total score is 100
+(float)getScore:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
//evaluate the body shape, 5 type body shape:
/*
 <0.5 Lean type
 0.5<= x <1.5 Normal Type
 1.5<= x <2.5 over weight type
 2.5<= x <3.5 over fat
 3.5<= x      Obese
 */
+(float)getBodyShape:(Boolean)sex age:(float)age cm:(float)cm kg:(float)kg imp:(float)imp althlevel:(int)althlevel;
@end
